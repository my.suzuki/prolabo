package com.tuyano.springboot.repositories;

import com.tuyano.springboot.Contacts;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ContactsRepository  extends JpaRepository<Contacts, Long> {

	Iterable<Contacts> findAll();
	
}
