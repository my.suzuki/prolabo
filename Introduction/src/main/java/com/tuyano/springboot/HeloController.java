package com.tuyano.springboot;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.tuyano.springboot.repositories.ContactsRepository;

@Controller
public class HeloController {

	@Autowired
	ContactsRepository repository;
	
	@RequestMapping("/")
	public ModelAndView index(ModelAndView mav) {
		mav.setViewName("index");
		return mav;
	}
	
	@RequestMapping("/greeting")
	public ModelAndView greeting(ModelAndView mav) {
		mav.setViewName("greeting");
		return mav;
	}
	
	@RequestMapping("/cafe")
	public ModelAndView cafe(ModelAndView mav) {
		mav.setViewName("cafe");
		return mav;
	}
	
	@RequestMapping("/jr")
	public ModelAndView jr(ModelAndView mav) {
		mav.setViewName("jr");
		return mav;
	}
	
	@RequestMapping(value="/contact", method = RequestMethod.GET)
	public ModelAndView contact(
		@ModelAttribute("formModel") Contacts contacts,
		ModelAndView mav) {
		mav.setViewName("contact");
		Iterable<Contacts> list = repository.findAll();
		mav.addObject("datalist", list);
		return mav;
	}
	
	@RequestMapping(value = "/contact", method = RequestMethod.POST)
	@Transactional(readOnly=false)
	public ModelAndView form(
			@ModelAttribute("formModel") Contacts contacts, 
			ModelAndView mav) {
		repository.saveAndFlush(contacts);
		return new ModelAndView("redirect:/");
	}

}
